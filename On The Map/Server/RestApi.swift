//
//  RestApi.swift
//  On The Map
//
//  Created by Juliano Alvarenga on 02/06/18.
//  Copyright © 2018 Juliano Alvarenga. All rights reserved.
//

import Foundation
import UIKit

struct ResApi {
    
    //MARK: - Creating UserSession
    
    func userSession(_ params: [String : AnyObject], _ controller: UIViewController, completion: @escaping (UserStruct) -> () ) {
        
        guard let vc = controller as? LoginVC else { return }
        
        let jsonData = try? JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)
        
        var request = URLRequest(url: URL(string: "https://www.udacity.com/api/session")!)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpBody = jsonData
        
        let session = URLSession.shared
        
        vc.activityIndicator.startAnimating()
        
        let task = session.dataTask(with: request) { data, response, error in
            
            DispatchQueue.main.async {
                
                vc.activityIndicator.stopAnimating()
                
                if error != nil { // Handle error…
                    
                    Singleton.shared.utillity.alert(title: "I,m sorry", msg: "Coudn't connect, please check your connection", externalController: vc)
                    
                }
                
                // Wrong credentials
                guard let statusCode = (response as? HTTPURLResponse)?.statusCode, statusCode >= 200 && statusCode <= 299 else {
                    
                    Singleton.shared.utillity.alert(title: "I'm sorry", msg: "Email of password not found.", externalController: vc)
                    return
                }
            }
            
            do {
                
                guard let localData = data else { return }
                let range = Range(5..<localData.count)
                
                guard let newData = data?.subdata(in: range) else { return }
                
                let _ = JSONDecoder.KeyDecodingStrategy.convertFromSnakeCase
                let session = try JSONDecoder().decode(UserStruct.self, from: newData)
                
                completion(session)
                print(session)
                
            } catch {
                
                print(error)
            }
            
        }
        
        task.resume()
        
        
    }
    
    //MARK: - RequestJSON List
    
    func getListLocation(_ controller: UIViewController, completion: @escaping (Results)-> ()) {
        
        guard let vc = controller as? MapVC else { return }
        
        var urlComponents = URLComponents(string: "https://parse.udacity.com/parse/classes/StudentLocation")
        
        urlComponents?.queryItems = [
            URLQueryItem(name: "limit", value: "100"),
            URLQueryItem(name: "order", value: "-updatedAt")
        ]
        
        guard let finalURL = urlComponents?.url else { return }
        
        var request = URLRequest(url: finalURL)
        
        request.addValue("QrX47CA9cyuGewLdsL7o5Eb8iug6Em8ye0dnAbIr", forHTTPHeaderField: "X-Parse-Application-Id")
        request.addValue("QuWThTdiRmTux3YaDseUSEpUKo7aBYM737yKd4gY", forHTTPHeaderField: "X-Parse-REST-API-Key")
        
        let session = URLSession.shared
        
        vc.activityIndicator.startAnimating()
        
        let task = session.dataTask(with: request) { (data, response, error) in
            
            DispatchQueue.main.async {
                
                vc.activityIndicator.stopAnimating()
                
                if error != nil {
                    
                    Singleton.shared.utillity.alert(title: "I,m sorry", msg: "Lost connection", externalController: vc)
                    return
                }
                
                guard let statusCode = (response as? HTTPURLResponse)?.statusCode, statusCode >= 200 && statusCode <= 299 else {
                    
                    Singleton.shared.utillity.alertApiError(title: "I'm sorry", msg: "Coudant Connect to Maps", externalContoller: vc)
                    
                    return
                }
                
            }
            
            do {
                
                guard let localData = data else { return }
                
                let _ = JSONDecoder.KeyDecodingStrategy.convertFromSnakeCase
                let json = try JSONDecoder().decode(Results.self, from: localData)
                
                completion(json)
                
            } catch {
                
                print(error)
            }
            
        }
        
        task.resume()
        
    }
    
    //MARL: - Request User Data
    
    func searchUserData(_ controller: UIViewController, _ completion: @escaping (Bool) -> () ) {
        
        guard let vc = controller as? DetailVC else { return }
        guard let userKey = Singleton.shared.session.account?.key else { return }
        
        let finalURL = URL(string: "https://www.udacity.com/api/users/\(userKey)")!
        
        let request = URLRequest(url: finalURL)
        
        let session = URLSession.shared
        
        vc.activityIndicator.startAnimating()
        
        let task = session.dataTask(with: request) { (data, response, error) in
            
            DispatchQueue.main.async {
                
                vc.activityIndicator.stopAnimating()
                
                if error != nil {
                    
                    Singleton.shared.utillity.alert(title: "I'm sorry", msg: "Lost connection", externalController: vc)
                    
                }
                
                guard let statusCode = (response as? HTTPURLResponse)?.statusCode, statusCode >= 200 && statusCode <= 299 else {
                    
                    Singleton.shared.utillity.alertApiError(title: "I'm sorry", msg: "Coudant Connect to Maps", externalContoller: vc)
                    
                    return
                }
                
            }
            
            do {

                guard let localData = data else { return }
                let range = Range(5..<localData.count)
                
                guard let newData = data?.subdata(in: range) else { return }
                
                let json = try JSONSerialization.jsonObject(with: newData, options: .allowFragments) as! [String:AnyObject]
                
                if let user = json["user"] as? [ String : AnyObject ] {
                    
                    if let lastName = user["last_name"] as? String {
                        
                        vc.lastNamelbl = lastName
                        
                    }
                    
                    if let name = user["nickname"] as? String  {
                        
                        vc.nameLbl = name
                        
                    }
                }
                
                completion(true)

            } catch {

                print(error)
            }
            
        }
        
        task.resume()
        
    }
    
    //MARK: - Logout
    
    func logout(_ controller: UIViewController) {
        
        guard let vc = controller as? MapVC else { return }
        
        var request = URLRequest(url: URL(string: "https://www.udacity.com/api/session")!)
        
        request.httpMethod = "DELETE"
        
        var xsrfCookie: HTTPCookie? = nil
        let sharedCookieStorage = HTTPCookieStorage.shared
        
        for cookie in sharedCookieStorage.cookies! {
            if cookie.name == "XSRF-TOKEN" { xsrfCookie = cookie }
        }
        
        if let xsrfCookie = xsrfCookie {
            request.setValue(xsrfCookie.value, forHTTPHeaderField: "X-XSRF-TOKEN")
        }
        
        let session = URLSession.shared
        
        vc.activityIndicator.startAnimating()
        
        let task = session.dataTask(with: request) { data, response, error in
            
            DispatchQueue.main.async {
                
                vc.activityIndicator.stopAnimating()
                
                if error != nil { // Handle error…
                    
                    Singleton.shared.utillity.alert(title: "I,m sorry", msg: "Lost connection", externalController: controller)
                    
                    return
                }
                
            }
            
            let range = Range(5..<data!.count)
            let newData = data?.subdata(in: range) /* subset response data! */
            print(String(data: newData!, encoding: .utf8)!)
            
        }
        
        task.resume()
    }
    
    
    //MARK: - ADDPIN
    
    func addPin(_ params: [String:AnyObject],_ controller: UIViewController, completion: @escaping () -> () ) {
        
        guard let vc = controller as? DetailVC else { return }
        
        let innerParams = params
        
        let jsonData = try? JSONSerialization.data(withJSONObject: innerParams, options: .prettyPrinted)
        
        var request = URLRequest(url: URL(string: "https://parse.udacity.com/parse/classes/StudentLocation")!)
        request.httpMethod = "POST"
        request.addValue("QrX47CA9cyuGewLdsL7o5Eb8iug6Em8ye0dnAbIr", forHTTPHeaderField: "X-Parse-Application-Id")
        request.addValue("QuWThTdiRmTux3YaDseUSEpUKo7aBYM737yKd4gY", forHTTPHeaderField: "X-Parse-REST-API-Key")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpBody = jsonData
        
        let session = URLSession.shared
        
        vc.activityIndicator.startAnimating()
        
        let task = session.dataTask(with: request) { data, response, error in
            
            DispatchQueue.main.async {
                
                vc.activityIndicator.stopAnimating()
                
                if error != nil { // Handle error…
                    
                    Singleton.shared.utillity.alert(title: "I,m sorry", msg: "Lost connection", externalController: vc)
                    
                    return
                }
                
                completion()
                
            }
            
        }
        
        task.resume()
        
    }
    
}

