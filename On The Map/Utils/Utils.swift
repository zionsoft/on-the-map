//
//  Utils.swift
//  On The Map
//
//  Created by Juliano Alvarenga on 02/06/18.
//  Copyright © 2018 Juliano Alvarenga. All rights reserved.
//

import Foundation
import UIKit


class Utils: UIViewController {
    
    //MARK: - Alert
    
    public func alert(title: String, msg: String, externalController: UIViewController) {
        
        DispatchQueue.main.async {
            
            let controller = UIAlertController(title: title, message: msg, preferredStyle: .alert)
            controller.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            externalController.present(controller, animated: true, completion: nil)
        }
       
    
    }
    
    public func alertLogout(title: String, msg: String, externalContoller: UIViewController) {
        
        let controller = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        controller.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: nil))
        controller.addAction(UIAlertAction(title: "Yes", style: .default, handler: {
            response in
            
            
            externalContoller.dismiss(animated: true, completion: {
                
                Singleton.shared.request.logout(self)
            
            })
            
        }))
        
        externalContoller.present(controller, animated: true, completion: nil)
        
    }
    
    public func alertApiError(title: String, msg: String, externalContoller: UIViewController) {
        
        let controller = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        controller.addAction(UIAlertAction(title: "Ok", style: .default, handler: {
            response in
            
            
            externalContoller.dismiss(animated: true, completion: {
                
                Singleton.shared.request.logout(self)
                
            })
            
        }))
        
        externalContoller.present(controller, animated: true, completion: nil)
        
    }
    
    public func alertWithCompletion(title: String, msg: String, externalContoller: UIViewController, _ completion: @escaping () -> () ) {
        
        let controller = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        controller.addAction(UIAlertAction(title: "Ok", style: .default, handler: {
            response in
            
            
            externalContoller.dismiss(animated: true, completion: {
                
                completion()
                
            })
            
        }))
        
        externalContoller.present(controller, animated: true, completion: nil)
        
    }
}
