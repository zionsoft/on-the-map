//
//  UserStruct.swift
//  On The Map
//
//  Created by Juliano Alvarenga on 4/15/18.
//  Copyright © 2018 Juliano Alvarenga. All rights reserved.
//

import Foundation

//MARK: - Studant Session

struct UserStruct: Decodable {
    
    var account: Account?
    var session: Session?
    
}

struct Account: Decodable {
    var registered: Bool
    var key: String
}

struct Session: Decodable {
    var id: String
    var expiration: String
}
