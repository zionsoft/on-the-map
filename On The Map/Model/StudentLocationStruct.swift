//
//  StudentLocationStruct.swift
//  On The Map
//
//  Created by Juliano Alvarenga on 4/15/18.
//  Copyright © 2018 Juliano Alvarenga. All rights reserved.
//

import Foundation

//MARK: - Results with list of pins on map

struct Results: Decodable {
    
    var results: [StudentLocationStruct]?
    
}

struct StudentLocationStruct: Decodable {
    
    var objectId: String?
    var uniqueKey: String?
    var firstName: String?
    var lastName:  String?
    var mapString: String?
    var mediaURL: String?
    var latitude: Double?
    var longitude: Double?
    var createdAt: String?
    var updatedAt: String?
    
}
