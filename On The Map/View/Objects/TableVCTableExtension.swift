//
//  TableVCTableExtension.swift
//  On The Map
//
//  Created by Juliano Alvarenga on 02/06/18.
//  Copyright © 2018 Juliano Alvarenga. All rights reserved.
//

import Foundation
import UIKit

//MARK: - Table Protocol functions

extension TableVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return Singleton.shared.studantPins.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "locationCell")
        
        let img = UIImage(named: "maps")
        
        guard let name = list[indexPath.row].firstName  else { return cell! }
        guard let lastName = list[indexPath.row].lastName else { return cell! }
        guard let detail = list[indexPath.row].mediaURL else { return cell! }
        
        cell?.imageView?.image = img
        cell?.textLabel?.text = "\(String(describing: name)) \(String(describing: lastName))"
        cell?.detailTextLabel?.text = detail
        
        return cell!
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        index = indexPath.row
        
        performSegue(withIdentifier: "showMedia", sender: self)
        
    }
    
}
