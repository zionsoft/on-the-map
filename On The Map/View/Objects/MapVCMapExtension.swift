//
//  MapVCMapExtension.swift
//  On The Map
//
//  Created by Juliano Alvarenga on 02/06/18.
//  Copyright © 2018 Juliano Alvarenga. All rights reserved.
//

import Foundation
import MapKit

//MARK: Mapkit Protocol Functions

extension MapVC: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        if let studantpin = map.dequeueReusableAnnotationView(withIdentifier: MKMapViewDefaultClusterAnnotationViewReuseIdentifier) as? MKMarkerAnnotationView {
            
            studantpin.animatesWhenAdded = true
            studantpin.titleVisibility = .adaptive
            studantpin.subtitleVisibility = .adaptive
            studantpin.canShowCallout = true
            studantpin.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
            
            return studantpin
            
        }
        
        return nil
        
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        
        guard let mediaUrl = view.annotation?.subtitle else { return }
        urlMedia = mediaUrl
        
        if urlMedia != "" {
            
            performSegue(withIdentifier: "showMedia", sender: self)
            
        } else {
            
            let controller = UIAlertController(title: "I'm sorry", message: "Theres no futher information about this studant.", preferredStyle: .alert)
            controller.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            present(controller, animated: true, completion: nil)
            
        }
        
    }
    
}
