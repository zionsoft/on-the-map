//
//  StudantPin.swift
//  On The Map
//
//  Created by Juliano Alvarenga on 4/16/18.
//  Copyright © 2018 Juliano Alvarenga. All rights reserved.
//

import MapKit

class StudantPin: NSObject, MKAnnotation {
    
    var coordinate: CLLocationCoordinate2D
    var title: String?
    var subtitle: String?
    
    init(coordinate: CLLocationCoordinate2D, title: String?, subtitle: String?) {
        
        self.coordinate = coordinate
        self.title = title
        self.subtitle = subtitle
        
        super.init()
        
    }
    
}
