//
//  SessionUSer.swift
//  On The Map
//
//  Created by Juliano Alvarenga on 06/05/18.
//  Copyright © 2018 Juliano Alvarenga. All rights reserved.
//

import Foundation
import UIKit

//MARK: Singleton can be called anywhere

final class Singleton {
    
    static let shared = Singleton()
    
    //MARK: - Request
    
    var request = ResApi()
    
    //MARk: - Objects Structs
    
    var session = UserStruct()
    
    var studantPins = [StudentLocationStruct]()
    
    //MARk: - Utils
    
    var utillity = Utils()

}
