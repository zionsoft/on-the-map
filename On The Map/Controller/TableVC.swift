//
//  TableVC.swift
//  On The Map
//
//  Created by Juliano Alvarenga on 4/15/18.
//  Copyright © 2018 Juliano Alvarenga. All rights reserved.
//

import UIKit

class TableVC: UIViewController {

    //MARK: IBOutlets and variables
    
    var list: [StudentLocationStruct]!
    var index: Int!
    
    @IBOutlet weak var linkTableview: UITableView!
    
    //MARK: - View Manipulation

    override func viewWillAppear(_ animated: Bool) {
        super .viewWillAppear(animated)

        list = Singleton.shared.studantPins
    }
    
    
    //MARK: - Prepare for segue
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        guard let destination = segue.destination as? WebVC else { return }
        destination.urlPassed = list[index].mediaURL
    }
    
    //MARK: - IBActions
    
    @IBAction func addPin(_ sender: UIBarButtonItem) {
        
        performSegue(withIdentifier: "addPin", sender: self)
    }
 
    @IBAction func logoutAction(_ sender: UIBarButtonItem) {
        
         Singleton.shared.utillity.alertLogout(title: "Logout", msg: "Are your sure?", externalContoller: self)
    }
    
}
