//
//  LoginVC.swift
//  On The Map
//
//  Created by Juliano Alvarenga on 4/14/18.
//  Copyright © 2018 Juliano Alvarenga. All rights reserved.
//

import UIKit

class LoginVC: UIViewController {
    
    //MARK: IBOutlets and variables
    
    @IBOutlet weak var superView: UIView!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    //MARK: View Manipulation
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupDelegate()
     
    }
    
    func setupDelegate() {
        
        guard let email = email else { return }
        guard let password = password else { return }
        
        email.delegate = self
        password.delegate = self
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        UIView.animate(withDuration: 0.3) {
            self.view.endEditing(true)
            self.superView.transform = CGAffineTransform(translationX: 0, y: 0)
        }
        
    }
    
    
    //MARK: - Creating a Session
    func creatingSession(user: String, password: String) {
        
        let params = ["udacity": ["username":"\(user)", "password" : "\(password)"]] as [String:AnyObject]
        
        Singleton.shared.request.userSession(params, self) { (studantSession) in
            
            Singleton.shared.session = studantSession
            
            DispatchQueue.main.async {
                
                self.performSegue(withIdentifier: "showMap", sender: self)
                
            }
            
        }
    
    }
    
    //MARK: - Prepare for segue
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let destination = segue.destination as? WebVC else { return }
        
        destination.urlPassed = "http://www.udacity.com.br"
    }
    
    //MARK: - IBActions
    
    @IBAction func loginBtn(_ sender: Any) {
       
        guard let user = email.text else { return }
        guard let password = password.text else { return }
        
        if user == "" || password == "" {
                       
            Singleton.shared.utillity.alert(title: "Attention", msg: "You must provide your credentials", externalController: self)

        } else {
            
            creatingSession(user: user, password: password)
            
        }
        
    }
    
    @IBAction func register(_ sender: UIButton) {
       
        performSegue(withIdentifier: "register", sender: self)
        
    }
    
}

extension LoginVC: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        UIView.animate(withDuration: 0.3) {
            self.superView.transform = CGAffineTransform(translationX: 0, y: -80)
        }
        
    }
    
}
