//
//  DetailVC.swift
//  On The Map
//
//  Created by Juliano Alvarenga on 4/16/18.
//  Copyright © 2018 Juliano Alvarenga. All rights reserved.
//

import UIKit
import MapKit

class DetailVC: UIViewController {
    
    //MARK: IBOutlets and variables
    
    @IBOutlet weak var myMapView: MKMapView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var listOfPins = [StudentLocationStruct]()
    
    var mediaLbl: String!
    var locationLbl: String!
    var nameLbl: String!
    var lastNamelbl: String!
    var latitude: Double!
    var longitude: Double!
    
    //MARK: View Manipulation
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Singleton.shared.request.searchUserData(self) { (done) in
            
            if done {
                
                self.findLocation()
                
            }
            
        }
        
        
    }
    
    //MARK: - Find Location on Map
    
    func findLocation() {
        
        let searchRequest = MKLocalSearchRequest()
        searchRequest.naturalLanguageQuery = locationLbl
        
        let activeSearch = MKLocalSearch(request: searchRequest)
        
        DispatchQueue.main.async {
            
             self.activityIndicator.startAnimating()
            
        }
        
        activeSearch.start { (response, error) in
            
            DispatchQueue.main.async {
                self.activityIndicator.stopAnimating()
                
            }
            
            if response == nil {
                
                Singleton.shared.utillity.alertWithCompletion(title: "I'm sorry", msg: "Couldn't find the location.", externalContoller: self, {
                    
                    self.navigationController?.popViewController(animated: true)
                    
                })
               
                
            } else {
            
                self.latitude = response?.boundingRegion.center.latitude
                self.longitude = response?.boundingRegion.center.longitude
                
                let _span = MKCoordinateSpanMake(0.075, 0.075)
                let annotation = MKPointAnnotation()
                annotation.title = self.nameLbl
                annotation.subtitle = self.mediaLbl
                annotation.coordinate = CLLocationCoordinate2D(latitude: self.latitude, longitude: self.longitude)

                self.myMapView.addAnnotation(annotation)
                
                let region = MKCoordinateRegion(center: annotation.coordinate, span: _span)
                
                self.myMapView.setRegion(region, animated: true)
                
            }
            
        }
        
    }

    //MARK: - Add new Location
    
    func addLocation() {
        
        guard let key = Singleton.shared.session.account?.key else { return }
        
        let params = ["uniqueKey": key, "firstName": nameLbl, "lastName" : lastNamelbl, "mapString" : locationLbl, "mediaURL" : mediaLbl, "latitude" : latitude, "longitude": longitude] as [String : AnyObject]
        
        Singleton.shared.request.addPin(params, self) {
            
            DispatchQueue.main.async {
                
                Singleton.shared.utillity.alertWithCompletion(title: "Great", msg: "Location added to map", externalContoller: self, {
                    
                    self.navigationController?.popToViewController(self, animated: true)
                    
                })
                
            }
            
        }
    }
    
    //MARK: - IBAction
    
    @IBAction func addLocationPin(_ sender: UIButton) {
        
        addLocation()
        
    }
    
}
