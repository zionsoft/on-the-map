//
//  MapVC.swift
//  On The Map
//
//  Created by Juliano Alvarenga on 4/14/18.
//  Copyright © 2018 Juliano Alvarenga. All rights reserved.
//

import UIKit
import MapKit

class MapVC: UIViewController {
    
    //MARK: - IBOutlets and variables
    
    @IBOutlet weak var map: MKMapView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var urlMedia: String?
    
    //MARK: View Manipulation
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        map.register(MKMarkerAnnotationView.self, forAnnotationViewWithReuseIdentifier: MKMapViewDefaultClusterAnnotationViewReuseIdentifier)
        getList(self)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        map.reloadInputViews()
        
    }
    
    //MARK: - List of Studants
    
    func getList(_ controller: UIViewController) {
     
        Singleton.shared.request.getListLocation(self) { (json) in
            
            guard let results = json.results else { return }
        
            for item in results {
                
                DispatchQueue.main.async {
                    
                    Singleton.shared.studantPins.append(item)
                    
                    guard let firstName = item.firstName else { return }
                    guard let lastName = item.lastName else { return }
                    guard let mediaUrl = item.mediaURL else { return }
                    guard let latitude = item.latitude else { return }
                    guard let longitude = item.longitude else { return }
                    let coordinates = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
                    let pin = StudantPin(coordinate: coordinates, title: "\(firstName) \(lastName)", subtitle: "\(mediaUrl)")
                    
                    self.map.addAnnotation(pin)
                    self.map.reloadInputViews()
                    
                    self.activityIndicator.stopAnimating()
                }
                
            }
            
        }
    }
    
    //MARK: -Prepare for segue
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "showMedia" {
            
            guard let destination = segue.destination as? WebVC else { return }
            destination.urlPassed = urlMedia
            
        }
        
    }
    
    //MARK: - IBActions
    
    @IBAction func logout(_ sender: UIBarButtonItem) {
        
        Singleton.shared.utillity.alertLogout(title: "Logout", msg: "Are your sure?", externalContoller: self)
        
    }
    
    @IBAction func addPin(_ sender: UIBarButtonItem) {
        
        performSegue(withIdentifier: "addPin", sender: self)
        
    }
    
    @IBAction func reloadMap(_ sender: UIBarButtonItem) {
        
        getList(self)
        
    }
}


