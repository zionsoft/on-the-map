//
//  WebVC.swift
//  On The Map
//
//  Created by Juliano Alvarenga on 4/28/18.
//  Copyright © 2018 Juliano Alvarenga. All rights reserved.
//

import UIKit
import WebKit

class WebVC: UIViewController {
    
    @IBOutlet weak var myWebView: UIWebView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var urlPassed: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        callWebview()
        
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        
        DispatchQueue.main.async {
            self.activityIndicator.startAnimating()
        }
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        
        DispatchQueue.main.async {
            self.activityIndicator.stopAnimating()
        }
        
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        
        alertDismiss(title: "I'm sorry", msg: "Could't load the page!")
        
    }
    
    
    func alertDismiss(title: String, msg: String) {
        
        let controller = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        controller.addAction(UIAlertAction(title: "Ok", style: .default, handler: {
            response in
            
            self.dismiss(animated: true, completion: {
                
                self.dismiss(animated: true, completion: nil)
            })
            
        }))
        
        present(controller, animated: true, completion: nil)
        
    }
    
    func callWebview() {
        
        if urlPassed == "" {
            
             alertDismiss(title: "I'm sorry", msg: "Could't load the page!")
        }
        
        guard let stringUrl = urlPassed else { return }
        
        guard let myURL = URL(string: stringUrl) else { return }
        
        let request = URLRequest(url: myURL)
       
        myWebView.loadRequest(request)
            
    }
    
    @IBAction func back(_ sender: UIBarButtonItem) {
        
        dismiss(animated: true, completion: nil)
        
    }
    
}

extension WebVC: UIWebViewDelegate{}
