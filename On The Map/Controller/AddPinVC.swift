//
//  AddPinVC.swift
//  On The Map
//
//  Created by Juliano Alvarenga on 4/16/18.
//  Copyright © 2018 Juliano Alvarenga. All rights reserved.
//

import UIKit

class AddPinVC: UIViewController {

    //MARK: IBOutlets and Variables
    
    @IBOutlet weak var superView: UIView!
    @IBOutlet weak var locationTextField: UITextField!
    @IBOutlet weak var mediaTextField: UITextField!
    
    //MARK: View Manipulation
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationTextField.delegate = self
        mediaTextField.delegate = self
        
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        UIView.animate(withDuration: 0.3) {
            self.view.endEditing(true)
            self.superView.transform = CGAffineTransform(translationX: 0, y: 0)
        }
        
    }
    
    //MARK: - prepare for segue
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        guard let destination = segue.destination as? DetailVC else { return }
        
        guard let location = locationTextField.text else { return }
        guard let media = mediaTextField.text else { return }
        
        destination.locationLbl = location
        destination.mediaLbl = media
        
    }
    
    //MARK: - IBActions
    
    @IBAction func cancel(_ sender: UIBarButtonItem) {
    
        dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func showDetail(_ sender: UIButton) {
        
        performSegue(withIdentifier: "showDetail", sender: self)
        
    }
    
}

extension AddPinVC: UITextFieldDelegate {
    
    internal func textFieldDidBeginEditing(_ textField: UITextField) {
        
        UIView.animate(withDuration: 0.3) {
            self.superView.transform = CGAffineTransform(translationX: 0, y: -80)
        }
        
    }
    
}
